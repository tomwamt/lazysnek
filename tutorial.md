# Tutorial

LazySnek is a horrible language and you should never use it. But if you want to, here's how!

### Expressions

LazySnek is an expression-oriented language, meaning (almost) all language constructs are 
expressions.

There are two types of expressions. Function calls are surrounded by parentheses, values are not. 
For example, `(print "Hello World")` is a function call, while `print` and `"Hello World"` are 
values.

A function call is composed of one or more expressions. The first expression is used as the function 
to call, while the rest are its arguments.

```
(print "2+2={}" (+ 2 2))
```

Note there are no infix operators in LazySnek; + is merely a function that adds two numbers.

### Lazy evaluation

Expressions passed as arguments to functions are not immediately evaluated. Instead, the function 
and the context it was declared in is stored and evaluated if and when the value they represent is 
needed.

When the interpreter begins, it attempts to evaluate the top level expression. Which other 
expressions get evaluated as a consequence depends on the details of the program.

### Values

There are several types of values.

* Numbers: a simple floating point number, e.g. 3.14.
* Strings: a piece of text. May be delimited by either 'single quotes' or "double quotes".
* Booleans: a value representing true or false.
* Lists: an ordered collection of several elements. Delimited by [square braces].
* Names: a name that has been bound to another expression.
* Callables: a function.
* Unit: a special value of which there is only one instance, denoted by empty parenthesis ().

### Lists

A list is an ordered collected of expressions, represented in a linked-list structure. There are 
actually two types of list: the empty list (represented by `[])` and a list created by contructing a 
list from an expression and another list (which may be the empty list). The expression is then 
prepended onto the list. The [square brace] syntax is  simply shorthand for the construction 
procedure.

To create a list, use the `::` function.

```
# evaluates to true
(= (:: 1 (:: 1 (:: 2 (:: 3 (:: 5 []))))) [1 1 2 3 5])
```

Note that lists are also lazy; the expressions in a list are not evaluated until needed.

To retreive the first expression from a list, use the `head` function. To retreive the rest, use the 
`tail` function.

```
# evaluates to 5
(head [5 4 3 2 1])

# evaluates to [4 3 2 1]
(tail [5 4 3 2 1])

# evaluates to []
(tail [5])

# ERROR
(head [])
```

### Bindings

Expressions can be bound to names using the syntax `let name be expr`

```
let x be 5

(print '{}' x) # prints "5.0"
```

Bindings may occur before any expression, and apply only to that expression and any expressions it 
contains. Additionally, any number of bindings may be declared before an expression.


```
let a be 5
let b be 7

# prints "6.0"
(print '{}'
	let c be (+ a b)
	let d be 2
	(/ c d)
)
```

Due to lazyness, the expressions are not evaluated when they are declared. However, this may be 
changed by using a exclamation point as in `let! name be expr`. This immediately evaluates the 
expression just before the expression it applies to is evaluated. This may be useful for functions 
with side-effects (such as printing to the console)

```
# prints "Forced!" even though x is never used.
let! x be (print "Forced!")
()
```

### User-defined functions

To create your own functions, use the `fun` builtin function. This function accepts a list of 
parameter names and an expression to evaluate when the function is called.

```
((fun [x] (* x x)) 4) # evaluates to 16.0
```

Bind your functions to names for easy reuse!

```
let square be (fun [x]
	(* x x)
)

(print '{}' (+ (square 3) (square 4))) # prints "25.0"
```

### Control flow

To create conditional expressions, use the `match` builtin function. This function accepts an 
expression to test, then one or more "pattern lists" to match against. Each pattern is a list of 
length two. The first item is a pattern, and the second is an expression. The expression is 
evaluated if the value being tested matches the pattern. The pattern list checks top to bottom and 
stops at the first pattern it matches. The specifics of patterns are covered in the next section.

```
let lights be 4

# prints "There are four lights!"
(match lights
	[4 (print "There are four lights!")]
	[5 (print "There are five lights.")]
	[_ (print "There are {} lights." lights)]
)
```

### Patterns

Patterns represent a structure to be matched against. They come in several variaties of varying 
complexity.

* Wildcard: Represented by a single underscore _. Matches anything. Often used as a default 
catch-all.
* Name: Represented by a valid name. Matches anything. Binds the matched value to that name.
* Constant: Represented by a constant value of a number, string, bool, or unit. Matches an 
expression that evaluates to that value.
* List: Represented by a list value, where each item in the list is another pattern. Matches a list 
where each item in the list matches the corresponding pattern. Used to deconstruct lists, or match 
agains an empty list
* Cons: Represented by (:: p1 p2), where p1 and p2 are patterns. Matches a list where the head of 
list matches p1 and the tail matches p2.

### Currying
Functions may be partially applied, producing a callable that accepts the remaining parameters. This 
may be useful for higher order functions.

NOTE: Builtin functions do not support currying.

```
let addThreeThings be (fun [a b c]
	(+ (+ a b) c)
)
let add5ToTwoThings be (addThreeThings 5)

# prints "11.0"
(print
	(add5ToTwoThings 2 4)
)
```

### Looping

"You never mentioned loops!" I can hear you saying. In LazySnek, the correct way to loop or iterate 
is with recursion. For example, to generate a list containing the number 0 until N

```
let range be (fun [n]
	(match n
		[0 []]
		[_  (:: n (range (- n 1)))]
	)
)
(range 5) # evaluates to [0 1 2 3 4]
```

Of course, as with any language, reckless recursion can lead to stack overflow. However, LazySnek 
support tail recursion elimination, which means properly designed functions will never overflow.

```
let reversed be (fun [list]
	let loop be (fun [li acc]
		(match li
			[[] acc]
			[(:: h t) (loop t (:: h acc))]
		)
	)
	(loop list [])
)
(reversed [1 2 3 4]) # [4 3 2 1]
```

Notice how the function actually creates and calls a tail recursive inner function that accepts an 
accumulator argument.
