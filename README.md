# LazySnek

A lazily evaluated functional programming language reminiscent of Lisp.

Implemented entirely in Python (for now)

```
let count be (fun [c]
	(match c
		[0 0]
		[_ (+ (count (- c 1)) c)]
	)
)

(print '{}' (count 5))
```

A 15 minute tutorial can be found [here](tutorial.md).

## Features

* *Lazy-Evaluated* - Expressions are not evaluated until they must be used
* *Dynamically typed* - No type declarations
* *Interpreted* - No compiler necessary
* *Tail Recursion Elimination* - No stack overflow on large tail recursions

## Implemented

* Bindings
* User-defined functions (lambdas)
* Branching (w/ pattern matching)
* Lists
* Currying (Partial application)

## TODO

* User-defined data types (structs)
* Modules & Import (w/ standard library)
* Interop with Python or other languages
* Better error reporting
* Error handling
