from collections import deque
import re
from evaluator import *


class Token:
	def __init__(self, typ, value):
		self.type = typ
		self.value = value

	def eq(self, typ, value=None):
		return self.type == typ and (value is None or self.value == value)


def scan(code):
	tokens = [
		('WHITESPACE', r'\s+'),
		('PAREN_L', r'\('),
		('PAREN_R', r'\)'),
		('BRACE_L', r'\['),
		('BRACE_R', r'\]'),
		('DOT', r'\.'),
		('BANG', r'!'),
		('LET', r'let'),
		('BE', r'be'),
		('NUMBERLIT', r'\d+(\.\d*)?'),
		('STRINGLIT', r"\'[^\n\r]*?\'|" r'\"[^\n\r]*?\"'),
		('BOOLLIT', r'true|false'),
		('COMMENT', r'#.*?(?=\n|$)'),
		('IDENT', r'[^0-9\s\[\]\(\)\.!][^\s\[\]\(\)\.!]*'),
		('DEFAULT', r'.')
	]

	tok_re = str.join('|', ('(?P<{}>{})'.format(name, regex) for name, regex in tokens))

	for match in re.finditer(tok_re, code):
		typ = match.lastgroup # name of group that matched
		value = match.group(typ) # value of match

		# if something was not caught by a real token
		if typ == 'DEFAULT':
			raise ScanError('Unexpected {}'.format(value))
		# else if it wasn't just whitespace or a comment
		elif typ != 'WHITESPACE' and typ != 'COMMENT':
			yield Token(typ, value) # return a new token

	yield Token('EOF', 'EOF')


def parse(tokens):
	la = deque()
	la.append(next(tokens))

	# looks at the next token, adding to the back of the queue
	def lookahead():
		t = next(tokens)
		la.append(t)
		return t

	# matches from the front of the queue
	def match(typ, value=None):
		if value is None and la[0].type == typ:
			value = la[0].value
		elif la[0].type == typ and la[0].value == value:
			pass
		else:
			raise RuntimeError(f'Expected a {typ}( {value} ), got a {la[0].type}( {la[0].value} )')
		la.popleft()
		if len(la) == 0:
			lookahead()
		return value

	def stmt():
		bindings = {}
		while la[0].eq('LET'):
			match('LET')
			force = la[0].eq('BANG')
			if force:
				match('BANG')
			name = match('IDENT')
			match('BE')
			val = expr()
			bindings[name] = Stmt({}, val, force=force)
		e = expr()
		return Stmt(bindings, e)

	def expr():
		if la[0].eq('PAREN_L'):
			if lookahead().type == 'PAREN_R':
				return value()

			match('PAREN_L')
			f = stmt()
			a = []
			while not la[0].eq('PAREN_R'):
				a.append(stmt())
			match('PAREN_R')
			return Expr(f, a)
		else:				
			return value()

	def value():
		if la[0].eq('PAREN_L'):
			match('PAREN_L')
			match('PAREN_R')
			t = 'unit'
			v = Unit
		elif la[0].eq('BRACE_L'):
			return listlit()
		elif la[0].eq('NUMBERLIT'):
			t = 'number'
			v = match('NUMBERLIT')
		elif la[0].eq('STRINGLIT'):
			t = 'string'
			v = match('STRINGLIT')
		elif la[0].eq('BOOLLIT'):
			t = 'bool'
			v = match('BOOLLIT')
		else:
			t = 'id'
			v = match('IDENT')
		return Value(t, v)

	def listlit():
		match('BRACE_L')
		vs = []
		while not la[0].eq('BRACE_R'):
			vs.append(stmt())
		match('BRACE_R')

		li = EmptyList
		while len(vs) > 0:
			li = ListNode(vs.pop(), li)
		return Value('list',  li)

	return stmt()


def main(filename):
	with open(filename) as f:
		code = f.read()

	# for tok in scan(code):
	# 	print(f'{tok.type}( {tok.value} )')

	# print(parse(scan(code)))
	# print()

	Evaluator().evaluate(parse(scan(code)))


if __name__ == '__main__':
	import sys
	main(sys.argv[1])
