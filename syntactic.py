# Syntactic Objects

class UnitT:
	def __str__(self):
		return '()'

Unit = UnitT()


class Stmt:
	def __init__(self, bindings, expr, *, force=False):
		self.bindings = bindings
		self.expr = expr
		self.parent = None

		self.force = force
		self.cachedValue = None

		for name in self.bindings:
			self.bindings[name].parent = self

		if isinstance(self.expr, Expr):
			self.expr.func.parent = self
			for a in self.expr.args:
				a.parent = self
		elif self.expr.type == 'list':
			for s in self.expr.value:
				s.parent = self

	def clone(self):
		bindings = {}
		for name in self.bindings:
			bindings[name] = self.bindings[name].clone()

		expr = self.expr.clone()
		s = Stmt(bindings, expr, force=self.force)
		s.parent = self.parent
		return s

	def get(self, name):
		#print(f'{name} -> {self.expr}: {self.bindings.keys()}')
		if name in self.bindings:
			return self.bindings[name]
		elif self.parent is not None:
			return self.parent.get(name)
		else:
			raise RuntimeError(f'{name} could not be resolved')

	def __str__(self):
		return str.join('', (f'let {name} be {self.bindings[name]}\n' for name in self.bindings)) + str(self.expr)


class Expr:
	def __init__(self, func, args):
		self.func = func
		self.args = args

	def clone(self):
		return Expr(self.func.clone(), [a.clone() for a in self.args])

	def __str__(self):
		return '(' + str(self.func) + ' ' + str.join(' ', map(str, self.args)) + ')'


class Value:
	def __init__(self, typ, value):
		self.type = typ
		self.value = value

	def clone(self):
		return self

	def __str__(self):
		if self.value is None:
			return '()'
		elif self.type == 'list':
			return '[' + str.join(' ', (str(v) for v in self.value)) + ']'
		else:
			return str(self.value)


class ListNode:
	def __init__(self, head, tail=None):
		self.head = head
		if tail is not None:
			self.tail = tail
		else:
			if self.head is None:
				return
			self.tail = EmptyList
		self.length = 1 + len(self.tail)

	def __iter__(self):
		current = self
		while current != EmptyList:
			yield current.head
			current = current.tail

	def __len__(self):
		return self.length

	def __str__(self):
		current = self
		res = '['
		while current != EmptyList:
			res += str(current.head) + ' '
			current = current.tail
		res += ']'
		return res

EmptyList = ListNode(None)
EmptyList.length = 0