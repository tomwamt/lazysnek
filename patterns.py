from syntactic import *

# Patterns

class ConstantPattern:
	def __init__(self, value):
		self.value = value

	def match(self, evaluator, stmt):
		value = evaluator.eval_stmt(stmt)
		if self.value == value:
			return True, {}
		else:
			return False, {}


class NamePattern:
	def __init__(self, name):
		self.name = name

	def match(self, evaluator, stmt):
		return True, {self.name: stmt}


class WildcardPattern:
	def match(self, evaluator, stmt):
		return True, {}


class ListPattern:
	def __init__(self, patterns):
		self.patterns = patterns

	def match(self, evaluator, stmt):
		value = evaluator.eval_stmt(stmt)

		if not isinstance(value, ListNode):
			return False, {}

		if len(self.patterns) != len(value):
			return False, {}

		bindings = {}
		for p, s in zip(self.patterns, value):
			success, b = p.match(evaluator, s)
			if success:
				bindings.update(b)
			else:
				return False, {}
		return True, bindings


class ConsPattern:
	def __init__(self, head, tail):
		self.headPattern = head # any pattern
		self.tailPattern = tail # {Name, Wildcard, List} patterns

	def match(self, evaluator, stmt):
		value = evaluator.eval_stmt(stmt)

		if not isinstance(value, ListNode):
			return False, {}

		bindings = {}
		s, b = self.headPattern.match(evaluator, value.head)
		if s:
			bindings.update(b)
		else:
			return False, {}

		# horribly hacky
		v = Value('list', value.tail)
		tail = Stmt({}, Value('unit', Unit))
		tail.expr = v
		tail.parent = stmt.parent
		s, b = self.tailPattern.match(evaluator, tail)
		if s:
			bindings.update(b)
		else:
			return False, {}

		return True, bindings