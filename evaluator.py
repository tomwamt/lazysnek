from patterns import *
from syntactic import *


# Runtime objects

class Callable:
	def __init__(self, params, retStmt):
		self.retStmt = retStmt
		if isinstance(params, list):
			self.params = params
		elif isinstance(params, Value) and params.type == 'list' and all(isinstance(a.expr, Value) and a.expr.type == 'id' for a in params.value):
			self.params = [p.expr.value for p in params.value]
		else:
			raise RuntimeError('Function parameters must be a list of identifiers')

	def call(self, evaluator, args):
		#if isinstance(self.retStmt.expr, Expr) and evaluator.eval_stmt(self.retStmt.expr.func, useScope) == self:

		n = len(args)
		if n > len(self.params):
			raise RuntimeError('Too many parameters passed')

		scope = self.retStmt.clone()
		for name, arg in zip(self.params[:n], args):
			scope.bindings[name] = arg

		if n == len(self.params):
			evaluator.callStack.append(self)

			val = evaluator.eval_stmt(scope)

			while isinstance(val, TailRecPassback):
				scope = self.retStmt.clone()
				for name, arg in zip(self.params, val.args):
					scope.bindings[name] = arg
				val = evaluator.eval_stmt(scope)

			evaluator.callStack.pop()
		else:
			val = Callable(self.params[n:], scope)

		return val


class NativeCall:
	def __init__(self, func):
		self.f = func

	def call(self, evaluator, args):
		evaluator.callStack.append(self)
		val = self.f(*args)
		evaluator.callStack.pop()
		return val


class NativeCallNoStack:
	def __init__(self, func):
		self.f = func

	def call(self, evaluator, args):
		val = self.f(*args)
		return val


class TailRecPassback:
	def __init__(self, args):
		self.args = args


# Evaluator

class Evaluator:
	def __init__(self):
		self.callStack = [None]

		self.glblScope = Stmt({}, Value('unit', Unit))

		def printf(fmt, *args):
			print(self.eval_stmt(fmt).format(*(self.eval_stmt(a) for a in args)))
			return Unit
		self.glblScope.bindings['print'] = Stmt({}, Value('call', NativeCall(printf)))
		self.glblScope.bindings['input'] = Stmt({}, Value('call', NativeCall(lambda a: input(self.eval_stmt(a)))))

		self.glblScope.bindings['+'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) + self.eval_stmt(b))))
		self.glblScope.bindings['-'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) - self.eval_stmt(b))))
		self.glblScope.bindings['*'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) * self.eval_stmt(b))))
		self.glblScope.bindings['/'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) / self.eval_stmt(b))))

		self.glblScope.bindings['>'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) > self.eval_stmt(b))))
		self.glblScope.bindings['<'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) < self.eval_stmt(b))))
		self.glblScope.bindings['>='] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) >= self.eval_stmt(b))))
		self.glblScope.bindings['<='] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) <= self.eval_stmt(b))))
		self.glblScope.bindings['='] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) == self.eval_stmt(b))))
		self.glblScope.bindings['<>'] = Stmt({}, Value('call', NativeCall(lambda a, b: self.eval_stmt(a) != self.eval_stmt(b))))

		self.glblScope.bindings['int'] = Stmt({}, Value('call', NativeCall(lambda a: float(int(self.eval_stmt(a))))))

		funcache = {}
		def fun(params, stmt):
			sig = (params.expr, stmt)
			if sig not in funcache:
				funcache[sig] = Callable(params.expr, stmt)
			return funcache[sig]
		self.glblScope.bindings['fun'] = Stmt({}, Value('call', NativeCall(fun)))

		def match(e, *branches):
			if not all(isinstance(a.expr, Value) and a.expr.type == 'list' and len(a.expr.value) == 2 for a in branches):
				raise RuntimeError('All branches of a match must be lists of length 2')

			for br in branches:
				success, bindings = self.eval_pattern(br.expr.value.head.expr).match(self, e)
				if success:
					s = br.expr.value.tail.head.clone()

					s.bindings.update(bindings)

					if isinstance(s.expr, Expr) and self.eval_stmt(s.expr.func) == self.callStack[-1]:
						return TailRecPassback(s.expr.args)
					else:
						return self.eval_stmt(s)

			raise RuntimeError('No match found')
		self.glblScope.bindings['match'] = Stmt({}, Value('call', NativeCallNoStack(match)))

		def format_(fmt, *args):
			fs = self.eval_stmt(fmt)
			return fs.format(*(self.eval_stmt(a) for a in args))
		self.glblScope.bindings['format'] = Stmt({}, Value('call', NativeCall(format_)))

		def do(*stmts):
			for s in stmts[:-1]:
				self.eval_stmt(s)

			s = stmts[-1]
			if isinstance(s.expr, Expr) and self.eval_stmt(s.expr.func) == self.callStack[-1]:
				return TailRecPassback(s.expr.args)
			else:
				return self.eval_stmt(s)
		self.glblScope.bindings['do'] = Stmt({}, Value('call', NativeCallNoStack(do)))

		# list functions
		self.glblScope.bindings['::'] = Stmt({}, Value('call', NativeCall(lambda h, t: ListNode(h, self.eval_stmt(t)))))
		self.glblScope.bindings['head'] = Stmt({}, Value('call', NativeCall(lambda l: self.eval_stmt(self.eval_stmt(l).head))))
		self.glblScope.bindings['tail'] = Stmt({}, Value('call', NativeCall(lambda l: self.eval_stmt(l).tail)))

	def evaluate(self, tree):
		tree.parent = self.glblScope
		self.eval_stmt(tree)

	def eval_stmt(self, stmt):
		#input(f'stmt = {str(stmt)} : {type(stmt.expr)}')
		if stmt.cachedValue is not None:
			return stmt.cachedValue

		for name in stmt.bindings:
			s = stmt.bindings[name]
			if s.force:
				self.eval_stmt(s)

		if isinstance(stmt.expr, Value):
			v = self.eval_value(stmt.expr, stmt)
		else:
			v = self.eval_expr(stmt.expr)
		stmt.cachedValue = v
		return v

	def eval_expr(self, expr):
		#input('expr: '+str(expr))
		return self.eval_stmt(expr.func).call(self, expr.args)

	def eval_value(self, value, scope):
		#input('value: '+str(value))
		if value.type == 'number':
			return float(value.value)
		elif value.type == 'string':
			return bytes(value.value[1:-1], 'utf8').decode('unicode_escape')
		elif value.type == 'unit':
			return Unit
		elif value.type == 'bool':
			return value.value == 'true'
		elif value.type == 'id':
			return self.eval_stmt(scope.get(value.value))
		elif value.type == 'call':
			return value.value
		elif value.type == 'list':
			return value.value

	def eval_pattern(self, expr):
		if isinstance(expr, Value):
			if expr.type in ['number', 'string', 'unit', 'bool']:
				return ConstantPattern(self.eval_value(expr, None))
			elif expr.type == 'id':
				if expr.value == '_':
					return WildcardPattern()
				else:
					return NamePattern(expr.value)
			elif expr.type == 'list':
				return ListPattern([self.eval_pattern(p.expr) for p in expr.value])
		else:
			f = expr.func.expr
			a = expr.args
			if isinstance(f, Value) and f.type == 'id':
				if f.value == '::':
					headPat = self.eval_pattern(a[0].expr)
					tailPat = self.eval_pattern(a[1].expr)
					if isinstance(tailPat, NamePattern) or isinstance(tailPat, WildcardPattern) or isinstance(tailPat, ListPattern):
						return ConsPattern(headPat, tailPat)

		raise RuntimeError(f'Invalid pattern: {expr}')
