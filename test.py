import os
import io
import sys
import traceback
import interpreter

def run_tests():
	realStdout = sys.stdout
	sys.stdout = io.StringIO()
	for filename in os.listdir('tests'):
		filename = 'tests/' + filename
		realStdout.write(f'Running {filename}...\n')
		realStdout.flush()
		lines = []
		with open(filename) as f:
			l = f.readline()
			while l.startswith('#EXPECT '):
				lines.append(l[8:])
				l = f.readline()

		interpreter.main(filename)

		sys.stdout.seek(0)
		results = sys.stdout.readlines()
		sys.stdout.truncate(0)
		sys.stdout.seek(0)
		if lines != results:
			realStdout.write(f'{filename} - Expected: {lines} Got: {results}\n')

if __name__ == '__main__':
	run_tests()
